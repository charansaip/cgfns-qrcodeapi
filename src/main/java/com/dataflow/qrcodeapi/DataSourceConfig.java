package com.dataflow.qrcodeapi;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class DataSourceConfig {
	@Autowired
	Environment environment;

	@Bean
	DataSource dataSource() {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName(environment.getProperty("driver"));
		driverManagerDataSource.setUrl(environment.getProperty("url_report"));
		driverManagerDataSource.setUsername(environment.getProperty("dbusername_report"));
		driverManagerDataSource.setPassword(environment.getProperty("dbpassword_report"));
		return driverManagerDataSource;
	}

}
