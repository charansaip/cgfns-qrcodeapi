package com.dataflow.qrcodeapi.service;

import com.dataflow.qrcodeapi.model.QrDetails;

public interface QrCodeService {


    public String getUserDetails(String authkey);

    public QrDetails getQrDetails(Integer caseId);

  
    public String insertQrDetails(Integer caseId, String quuid, String codeToken,Integer user);
    
    public String updateQrDetails(Integer caseId, String quuid, String codeToken,Integer user);

	public String getPassport(Integer caseId);

	public String getCheckStatus(Integer caseId);

	public String getStatus(String checkStatus);

	public String getBarcode(Integer caseId);


   


}
