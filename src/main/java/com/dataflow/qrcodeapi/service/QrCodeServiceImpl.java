package com.dataflow.qrcodeapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataflow.qrcodeapi.dao.QrCodeDao;
import com.dataflow.qrcodeapi.model.QrDetails;

@Service
public class QrCodeServiceImpl implements QrCodeService{


    private static final String String = null;
	@Autowired
    private QrCodeDao qrcodedao;

    @Override
    public String getUserDetails(String authkey) {
        return qrcodedao.getUserDetails(authkey);
    }

    @Override
    public QrDetails getQrDetails(Integer caseId) {
        return qrcodedao.getQrDetails(caseId);

    }

    



	@Override
	public String insertQrDetails(Integer caseId, String quuid, String codeToken, Integer user) {
		return qrcodedao.insertQrDetails(caseId,quuid,codeToken,user);
	}

	

	@Override
	public String updateQrDetails(Integer caseId, java.lang.String quuid, java.lang.String codeToken,
			Integer user) {
		// TODO Auto-generated method stub
		return qrcodedao.updateQrDetails(caseId,quuid,codeToken,user);
	}

	@Override
	public String getPassport(Integer caseId) {
		// TODO Auto-generated method stub
		return qrcodedao.getPassport(caseId);
	}

	@Override
	public String getCheckStatus(Integer caseId) {
		// TODO Auto-generated method stub
		return qrcodedao.getCheckStatus(caseId);
	}

	@Override
	public String getStatus(String checkStatus) {
		// TODO Auto-generated method stub
		return qrcodedao.getStatus(checkStatus);
	}

	@Override
	public String getBarcode(Integer caseId) {
		// TODO Auto-generated method stub
		return qrcodedao.getBarcode(caseId);
	}

}
