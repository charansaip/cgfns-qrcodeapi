package com.dataflow.qrcodeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication
//@PropertySource(value = {"file:///C:/config/QRCodeAPI/QrCodeApiProperties.properties"}, ignoreResourceNotFound = true)

@PropertySources({@PropertySource(value = {"file:///C:/config/QRCodeAPI/QrCodeApiProperties.properties"}, ignoreResourceNotFound = true), 
	  @PropertySource(value = {"file:///appdata/config/QRCodeAPI/QrCodeApiProperties.properties"}, ignoreResourceNotFound = true)})
public class QrcodeapiApplication extends SpringBootServletInitializer {
	
	@Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
      return application.sources(QrcodeapiApplication.class);
  }




	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(QrcodeapiApplication.class, args);

	}
	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOriginPattern("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
//		config.addAllowedMethod("PUT");
//		config.addAllowedMethod("DELETE");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}
}



