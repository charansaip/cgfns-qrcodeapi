package com.dataflow.qrcodeapi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.dataflow.qrcodeapi.model.QrDetails;

@Component
@PropertySources({
	@PropertySource(value = {"file:///C:/config/QRCodeAPI/queries.xml"}, ignoreResourceNotFound = true),
	@PropertySource(value = {
			"file:///appdata/config/QRCodeAPI/queries.xml" }, ignoreResourceNotFound = true) })
public class QrCodeDaoImpl implements QrCodeDao{


    @Autowired
    private DataSource dataSource;

    @Autowired
    private Environment env;

    @Override
    public String getUserDetails(String authkey) {
        String user = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try (Connection connection = this.dataSource.getConnection();) {

            preparedStatement = connection.prepareStatement(this.env.getProperty("GET_USER_DETAIL_BY_AUTHKEY"));
            preparedStatement.setString(1, String.valueOf(authkey));
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user = resultSet.getString("USER_NAME");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if(user != null){
            return user;
        }else{
            return null;
        }
    }

    @Override
    public QrDetails getQrDetails(Integer caseId) {
        QrDetails qrDetails = new QrDetails();     
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try (Connection connection = this.dataSource.getConnection();) {

            //check in db whether the fields exist or not
            preparedStatement = connection.prepareStatement(this.env.getProperty("GET_DETAILS_FROM_REPORT_QRCODE_DETAILS"));
            preparedStatement.setInt(1, caseId);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
            	qrDetails.setCaseId(resultSet.getInt("CASE_ID"));
                qrDetails.setQuuid(resultSet.getString("quuid"));
                qrDetails.setCodeToken(resultSet.getString("code_token"));
                qrDetails.setCreatedDate(resultSet.getString("createdDate"));
                qrDetails.setCreatedBy(resultSet.getInt("createdBy"));
                qrDetails.setUpdatedDate(resultSet.getString("updatedDate"));
                qrDetails.setUpdatedBy(resultSet.getInt("updatedBy"));
                String passport = getPassport(caseId);
                qrDetails.setPassport(passport);
                String checkStatus = getCheckStatus(caseId);
                String status = getStatus(checkStatus);
				
                qrDetails.setStatus(status);
                String barcode = getBarcode(caseId);
                qrDetails.setBarcode(barcode);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return qrDetails;
    }

   

    @Override
    public String insertQrDetails(Integer caseId, String quuid, String codeToken,Integer user) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int response = 0;
       
        	try (Connection connection = this.dataSource.getConnection();) {
        		Date currentDate = new Date();
        		java.sql.Date createdDate = new java.sql.Date(currentDate.getTime());

                //check in db whether the fields exist or not
                preparedStatement = connection.prepareStatement(this.env.getProperty("INSERT_INTO_REPORT_QRCODE_DETAILS"));
                preparedStatement.setInt(1, caseId);
                preparedStatement.setString(3, quuid);
                preparedStatement.setString(2, codeToken);
                preparedStatement.setDate(4,createdDate);
                preparedStatement.setInt(5, user);
                response = preparedStatement.executeUpdate();


            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            if(response != 0){
                return "Successfully Inserted";
            }else{
                return "Not Valid";
            }
        	
       
		
        
    }

	@Override
	public String updateQrDetails(Integer caseId, String quuid, String codeToken, Integer user) {
		 PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	        int response = 0;
	       
		try (Connection connection = this.dataSource.getConnection();) {
    		Date currentDate = new Date();
    		java.sql.Date updatedDate = new java.sql.Date(currentDate.getTime());
    		
            //check in db whether the fields exist or not
            preparedStatement = connection.prepareStatement(this.env.getProperty("UPDATE_INTO_REPORT_QRCODE_DETAILS"));

            preparedStatement.setDate(1, updatedDate);
            preparedStatement.setInt(2, user);
            preparedStatement.setInt(3, caseId);
            preparedStatement.setString(4, quuid);
           
            response = preparedStatement.executeUpdate();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if(response != 0){
            return "Successfully Updated";
        }else{
            return "Not Valid";
        }
	}
	
	@Override
	public String getPassport(Integer caseId) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String passport="";
		Connection dbConnection = null;
			try (Connection connection = this.dataSource.getConnection();) {

	            //check in db whether the fields exist or not
	            preparedStatement = connection.prepareStatement(this.env.getProperty("GET_PASSPORT_FROM_APP_USER_DETAILS"));
	            preparedStatement.setInt(1, caseId);
	            resultSet = preparedStatement.executeQuery();
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				passport = resultSet.getString("PASSPORTCURRENTNO");
			}
			return passport;
		}catch (SQLException e) {
            throw new RuntimeException(e);
        }

	}

	@Override
	public String getCheckStatus(Integer caseId) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkStatus="";
		Connection dbConnection = null;
			try (Connection connection = this.dataSource.getConnection();) {

	            //check in db whether the fields exist or not
	            preparedStatement = connection.prepareStatement(this.env.getProperty("GET_CHECKSTATUS_FROM_CHECK_SUMMARY"));
	            preparedStatement.setInt(1, caseId);
	            resultSet = preparedStatement.executeQuery();
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				checkStatus = resultSet.getString("CHECKSTATUS_ID");
				
			
				
			}
			return checkStatus;
		}catch (SQLException e) {
            throw new RuntimeException(e);
        }

	}

	@Override
	public String getStatus(String checkStatus) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String status="";
		Connection dbConnection = null;
			try (Connection connection = this.dataSource.getConnection();) {

	            preparedStatement = connection.prepareStatement(this.env.getProperty("GET_STATUS_FROM_CHECK_SUMMARY"));
	            preparedStatement.setString(1, checkStatus);
	            resultSet = preparedStatement.executeQuery();
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				status = resultSet.getString("NAME");
				
				if (status.equalsIgnoreCase("Verified-Clear")) {
					status ="POSITIVE";
				}
				else if(status.equalsIgnoreCase("Unable To Verify") || status.equalsIgnoreCase("UTV")) {
					status = "UNABLE TO VERIFY";
				}
				else if (status.equalsIgnoreCase("Negative")) {
					status = "DISCREPANCY";
				}
			}
			return status;
		}catch (SQLException e) {
            throw new RuntimeException(e);
        }

	}

	@Override
	public String getBarcode(Integer caseId) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String barcode="";
		Connection dbConnection = null;
			try (Connection connection = this.dataSource.getConnection();) {

	            preparedStatement = connection.prepareStatement(this.env.getProperty("GET_BARCODE_FROM_CASE_DETAILS"));
	            preparedStatement.setInt(1, caseId);
	            resultSet = preparedStatement.executeQuery();
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				barcode = resultSet.getString("BARCODE");
				
				
			}
			return barcode;
		}catch (SQLException e) {
            throw new RuntimeException(e);
        }

	}


    



}
