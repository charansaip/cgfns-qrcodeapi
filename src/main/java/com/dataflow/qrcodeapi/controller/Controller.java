package com.dataflow.qrcodeapi.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dataflow.qrcodeapi.model.QrDetails;
import com.dataflow.qrcodeapi.service.QrCodeService;

@CrossOrigin(origins = "*",allowedHeaders = "*")
@RestController
@RequestMapping(value="/qrcode")
public class Controller {

    @Autowired
    private QrCodeService qrcodeservice;
    
	private static final Logger LOGGER = Logger.getLogger(Controller.class);
	 @GetMapping(value = "/healthcheck")
	    public ResponseEntity<?> getQuuidForFinalreport(@RequestHeader HttpHeaders headers   )   {
         return ResponseEntity.status(HttpStatus.OK).body("success");

	 }


    @GetMapping(value = "/{caseId}")
    public ResponseEntity<?> getQuuidForFinalreport(@RequestHeader HttpHeaders headers, @PathVariable("caseId") Integer caseId) throws SQLException {
    	QrDetails qrDetails=null ;
    	LOGGER.info("caseId"+caseId);
        //authentication
        List<String> authkey = headers.get("authkey");
        LOGGER.info("user"+authkey);
        String key = authkey.get(0);
        //auth validation script service
        String user = null;
        user = qrcodeservice.getUserDetails(key);
        

        if(user != null){
        	
        		   qrDetails = qrcodeservice.getQrDetails(caseId);
        		   String barcode = qrcodeservice.getBarcode(caseId);
   	            String passport = qrcodeservice.getPassport(caseId);
   	            
   	           String checkStatus = qrcodeservice.getCheckStatus(caseId);
   	           
   	           String status = qrcodeservice.getStatus(checkStatus);

                  LOGGER.info("QRDETAILS"+qrDetails);
                      if(qrDetails.getQuuid() != null){
//                    	    String qrInfo = "QR Code Type: " + qrDetails.getCodeType() + "\nCreation Date: " + qrDetails.getCreationDate();
//                    	    String combinedDetails = "QR Details:\n" + qrInfo + "\nPassport: " + passport;
//                    	   
                          return ResponseEntity.status(HttpStatus.OK).body(qrDetails);
                      }else{
                          return ResponseEntity.status(HttpStatus.OK).body("New CaseId Proceed To Generate Qryptal Api." + "\n"+"Passport Number : "+passport+ "\n"+"Status : " + status +"\n" + "Barcode : " + barcode);
                      
                      }
          
        }else{
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid Auth Key");
        }
    }
    
//    public String getQuuidForFinalreport(
//    	    @RequestHeader HttpHeaders headers,
//    	    @PathVariable("caseId") Integer caseId
//    	) throws SQLException {
//    	    QrDetails qrDetails = null;
//    	    LOGGER.info("caseId" + caseId);
//
//    	    List<String> authKey = headers.get("authKey");
//    	    LOGGER.info("user" + authKey);
//    	    String key = authKey.get(0);
//
//    	    String user = null;
//    	    user = qrcodeservice.getUserDetails(key);
//
//    	    if (user != null) {
//    	        qrDetails = qrcodeservice.getQrDetails(caseId);
//    	        LOGGER.info("QRDETAILS" + qrDetails);
//
//    	        if (qrDetails.getQuuid() != null) {
//    	            return ResponseEntity.status(HttpStatus.OK).body(qrDetails).toString();
//    	        } else {
//    	            return ResponseEntity.status(HttpStatus.OK).body("New CaseId Proceed To Generate Qryptal Api").toString();
//    	        }
//    	    } else {
//    	        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid Auth Key").toString();
//    	    }
//    	}
    
//    @GetMapping("/finalreport/{caseId}")
//    public Map<String, Object> getQuuidForFinalreport(
//    	    @RequestHeader HttpHeaders headers,
//    	    @PathVariable("caseId") Integer caseId
//    	) {
//    	    Map<String, Object> response = new HashMap<>();
//    	    try {
//    	        LOGGER.info("caseId: {}");
//
//    	        List<String> authKey = headers.get("authKey");
//    	        LOGGER.info("user: {}");
//    	        String key = authKey.get(0);
//
//    	        String user = qrcodeservice.getUserDetails(key);
//
//    	        if (user != null) {
//    	            QrDetails qrDetails = qrcodeservice.getQrDetails(caseId);
//    	            LOGGER.info("QRDETAILS");
//
//    	            if (qrDetails != null && qrDetails.getQuuid() != null) {
//    	                response.put("status", HttpStatus.OK);
//    	                response.put("data", qrDetails);
//    	            } else {
//    	                response.put("status", HttpStatus.OK);
//    	                response.put("data", "New CaseId Proceed To Generate Qryptal Api");
//    	            }
//    	        } else {
//    	            response.put("status", HttpStatus.UNAUTHORIZED);
//    	            response.put("data", "Invalid Auth Key");
//    	        }
//    	    } catch (Exception e) {
//    	        LOGGER.error("Exception occurred: {}");
//    	        response.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
//    	        response.put("data", "An error occurred while processing the request");
//    	        // Handle or return the response as needed
//    	    }
//
//    	    return response;
//    	}




       
    
    
    
    @PostMapping(value = "/{caseId}/{quuid}/{code_token}/{createdUser}")
    public ResponseEntity<?> insertQuuidForFinalReport(@RequestHeader HttpHeaders headers,@PathVariable("caseId") Integer caseId,@PathVariable("quuid") String quuid,@PathVariable("code_token") String codeToken,@PathVariable("createdUser") Integer createdUser){
        //Authentication of user
    	LOGGER.info("caseId"+caseId+"quuid"+quuid+"code_token"+codeToken+"createdUser"+createdUser);
        List<String> authkey = headers.get("authkey");
        LOGGER.info("authey "+authkey);
        String key = authkey.get(0);
        //auth validation script - working
        String user = null;
        user = qrcodeservice.getUserDetails(key);
        //checking in Db if the same caseId exist
        QrDetails qrDetails = qrcodeservice.getQrDetails(caseId);
        LOGGER.info("qrDetails"+qrDetails);
        String quuidForVerification = qrDetails.getQuuid();
        LOGGER.info("quuidForVerification"+quuidForVerification);
        String message = null;
        if(user != null && quuidForVerification == null){
            message = qrcodeservice.insertQrDetails(caseId,quuid,codeToken,createdUser);
        }else if(user == null){
            message = "Invalid Auth Key";
        }else {
            message = "Case Id already exists";
        }
        return ResponseEntity.status(HttpStatus.OK).body(message);
    }
    
    @PostMapping(value = "/update/{caseId}/{quuid}/{code_token}/{updatedUser}")
    public ResponseEntity<?> updateQuuidForFinalReport(@RequestHeader HttpHeaders headers,@PathVariable("caseId") Integer caseId,@PathVariable("quuid") String quuid,@PathVariable("updatedUser") Integer updatedUser,@PathVariable("code_token") String codeToken){
        LOGGER.info("caseId "+caseId+"quuid "+quuid+"codeToken "+codeToken+"updatedUser "+updatedUser);
    	//Authentication of user
        List<String> authkey = headers.get("authkey");
        LOGGER.info("authey "+authkey);
        String key = authkey.get(0);
        //auth validation script - working
        String user = null;
        user = qrcodeservice.getUserDetails(key);
        //checking in Db if the same caseId exist
        QrDetails qrDetails = qrcodeservice.getQrDetails(caseId);
        String quuidForVerification = qrDetails.getQuuid();
        String message = null;
        if(user != null && quuidForVerification !=null){
            message = qrcodeservice.updateQrDetails(caseId,quuid,codeToken,updatedUser);
        }else if(user == null){
            message = "Invalid Auth Key";
        }else {
            message = "Invalid Case Id";
        }
        return ResponseEntity.status(HttpStatus.OK).body(message);
    }
    
}

