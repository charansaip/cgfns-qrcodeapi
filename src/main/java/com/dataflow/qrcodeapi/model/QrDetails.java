package com.dataflow.qrcodeapi.model;

public class QrDetails {



    public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	private Integer caseId;

    private String quuid;
    private String codeToken;
    private String createdDate;
    private Integer createdBy;
    
    private String passport;
    
    private String status;
    
    private String barcode;
    
    public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

//	@Override
//	public String toString() {
//		return "QrDetails [caseId=" + caseId + ", quuid=" + quuid + ", codeToken=" + codeToken + ", createdDate="
//				+ createdDate + ", createdBy=" + createdBy + ",  updatedDate=" + updatedDate
//				+ ", updatedBy=" + updatedBy + "]";
//	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	private String updatedDate;
    private Integer updatedBy;

    public Integer getCaseId() {
		return caseId;
	}

	public void setCaseId(Integer caseId) {
		this.caseId = caseId;
	}

	public String getQuuid() {
		return quuid;
	}

	public void setQuuid(String quuid) {
		this.quuid = quuid;
	}

	public String getCodeToken() {
		return codeToken;
	}

	public void setCodeToken(String codeToken) {
		this.codeToken = codeToken;
	}

	

	



}
